#!/usr/bin/env python

import numpy as np
import math
import sys

def isDecompositionInRank(translation, rank, size, trans_x, trans_y):
    min_trans_x, max_trans_x = trans_x
    min_trans_y, max_trans_y = trans_y

    size_x = int(math.sqrt(size))
    size_y = int(math.ceil(float(size) / size_x))
    last_row = int(size - (size_x * (size_y - 1)))

    my_index_y = int(rank / size_x)
    my_index_x = int(rank % size_x)
    
    frac_y = (translation[1] - min_trans_y) / ((max_trans_y - min_trans_y) * (1 + sys.float_info.epsilon))
    pos_y = int(frac_y * size_y)
    
    if (pos_y < 0):
        pos_y = 0
    if (pos_y >= size_y):
        pos_y = size_y - 1;
    
    frac_x = (translation[0] - min_trans_x) / ((max_trans_x - min_trans_x) * (1 + sys.float_info.epsilon))
    
    if (pos_y == size_y - 1):
        pos_x = int(frac_x * last_row)
        if (pos_x >= last_row):
            pos_x = last_row - 1
    else:
        pos_x = int(frac_x * size_x)
        if (pos_x >= size_x):
            pos_x = size_x - 1
    
    if (pos_x < 0):
        pos_x = 0
    
    if (pos_x == my_index_x and pos_y == my_index_y):
        return True
    
    return False
    
def calculateDecomposition(rank, n_tiles, size, translations):
    my_frames = []
    
    if (size == 1):
        return [i for i in range(translations.shape[0])]
    
    min_trans_x = translations[:, 0].min()
    max_trans_x = translations[:, 0].max()
    
    min_trans_y = translations[:, 1].min()
    max_trans_y = translations[:, 1].max()
    
    # print(min_trans_x, max_trans_x, min_trans_y, max_trans_y)
    
    size_x = int(math.sqrt(size))
    size_y = int(math.ceil(float(size) / size_x))
    last_row = int(size - (size_x * (size_y - 1)))

    trans_x = (min_trans_x, max_trans_x)
    trans_y = (min_trans_y, max_trans_y)
    
    for i in range(translations.shape[0]):
        for r in range(rank, rank + n_tiles):
            if isDecompositionInRank(translations[i], r, size, trans_x, trans_y):
                my_frames.append(i)
    return my_frames
       
    
    



    
