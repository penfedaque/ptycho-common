#!/usr/bin/env python

import sys, getopt
from .misc import printv
from mpi4py import *


help =   "\nsharpy [options] input.cxi\n\n\
-s SOLV -> Specifies the solver to use. SOLV=0 employs RAAR (default), SOLV=1 employs ADMM.\n\
-i ITER -> Run for ITER iterations. Defaults to 10.\n\
-r ITER -> Refine the illumination every ITER iterations. Defaults to no refinement.\n\
\
\
\nAdvanced Options:\n\n\
-b BETA -> Beta parameter for RAAR. Defaults to 0.6.\n\
-p RONE -> Step size for the ADMM solver. \
		\n\tBigger RONE equals slower and more robust convergence. Defaults to 0.5.\n\
-T ITER -> Recalculate background every ITER iterations. Off by default.\n\
-A -> Use ADP (Advanced Denoising for X-ray Ptychography) for the background characterization and removal. \
		\n\t Using this option employs ADMM as the framework algorithm. \n\
-M -> \tEnforce fourier mask when refining illumination.\n\
-C FACTOR -> \tCrop the reconstruction image by 2D_frame_size.x,y / FACTOR. Defaults to 2. Set to 0 to disable.\n\
-I OBJ_MASK_PATH -> \tEmploy a given binary mask as a constraint for the object in real space.\n\
\
-Y USCAL -> USCAL specifies an image regularization scaling factor. Defaults to  1e-2f.\n\
\
-y WSCAL -> WSCAL specifies an illumination regularization scaling factor. Defaults to  1e-2f.\n\
\
-G NGPUranks -> Specifies how many MPI ranks will employ GPU acceleration. MPI_size - NGPUranks = MPI CPU computation processes\n"


def parse_arguments(args, options = None):

    printv("Parsing parameters...")

    if options is None:

        #default
        options = {
                     "iterations": 50,
                     "illumination_ref": 0, 
                     "background_ref": 0,
                     "solver": "RAAR",
                     "RAAR_beta": 0.6,
                     "bck_type": 1,
                     "enable_illum_mask": False,
                     "crop_factor": 2,
                     "n_GPU_ranks": MPI.COMM_WORLD.Get_size()
                  }

    try:
        opts, args_left = getopt.getopt(args,"hs:T:i:r:Ab:Mp:Y:y:C:I:G:", \
                              ["solver=","bkg_ref=","iterations=","illum_ref=","ADP","RAAR_beta=","illum_mask", "ADMM_penalty=", "reg_scal_image=", "reg_scal_illum=", "crop=", "image_mask=", "n_gpus="])

    except getopt.GetoptError:
        printv(help)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printv(help)
            sys.exit()
        elif opt in ("-i", "--iterations"):
            options["iterations"] = int(arg)

        elif opt in ("-s", "--solver"):
            if int(arg) is 0:
                options["solver"] = "RAAR"
            elif int(arg) is 1:
                options["solver"] = "ADMM"

        elif opt in ("-T", "--bkg_ref"):
            options["background_ref"] = int(arg)

        elif opt in ("-r", "--illum_ref"):
            options["illumination_ref"] = int(arg)

        elif opt in ("-M", "--illum_mask"):
            options["enable_illum_mask"] = True

        elif opt in ("-A", "--ADP"):
            options["bck_type"] = 3

        elif opt in ("-b", "--RAAR_beta"):
            options["RAAR_beta"] = float(arg)

        elif opt in ("-p", "--ADMM_penalty"):
            options["ADMM_r"] = float(arg)

        elif opt in ("-Y", "--reg_scal_image"):
            options["regularizer_scaling_u"] = float(arg)

        elif opt in ("-y", "--reg_scal_illum"):
            options["regularizer_scaling_w"] = float(arg)

        elif opt in ("-C", "--crop"):
            options["crop_factor"] = int(arg)

        elif opt in ("-I", "--image_mask"):
            options["image_mask"] = str(arg)

        elif opt in ("-G", "--n_gpus"):
            options["n_GPU_ranks"] = str(arg)

    if options["bck_type"] is 3:
        options["solver"] = "ADMM"

    if len(args_left) is not 1:

        printv(help)
        sys.exit(2)

    else:
        options["fname"] = args_left[0]

    return options


    
