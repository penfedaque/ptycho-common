#!/usr/bin/env python

import h5py
import numpy as np
from .misc import printv

class IO:

    def __init__(self):

        groups = {
                "tomography": "tomography/",
                "data": "entry_1/data_1/",
                "geometry": "entry_1/sample_1/geometry_1/",
                "source":  "entry_1/instrument_1/source_1/", 
                "detector": "entry_1/instrument_1/detector_1/",
                "process": "entry_1/image_1/process_1/"
            }
 
        self.dataFormat = {"data": [groups["data"] + "data"]} #3D if experiment is 2D ptychography, 4D if it is ptycho-tomography               

        self.metadataFormat = {

                #Tomography experimental fields

                "angles": [groups["tomography"] + "angles"],

                #Ptychography experimental fields 

                "translations": [groups["geometry"] + "translation"], #translations in meters
                "pix_translations": [groups["geometry"] + "pix_translations"], #translations in pixels
                "energy": [groups["source"] + "energy"],
                "illumination": 
                    [groups["source"] + "illumination", 
                    groups["source"] + "probe",
                    groups["detector"] + "probe",
                    groups["detector"] + "data_illumination"],
                "detector_distance": [groups["detector"] + "distance"],
                "illumination_distance": [groups["source"] + "illumination_distance"],
                "x_pixel_size": [groups["detector"] + "x_pixel_size"],
                "y_pixel_size": [groups["detector"] + "y_pixel_size"],
                "illumination_mask":
                    [groups["source"] + "probe_mask",
                     groups["detector"] + "probe_mask"],

                "illumination_intensities":
                    [groups["source"] + "illumination_intensities",
                     groups["detector"] + "illumination_intensities"],

                "near_field": [groups["detector"] + "near_field"],
                "pinhole_width": [groups["source"] + "pinhole_width"],
                "phase_curvature": [groups["source"] + "phase_curvature"],

                #Ptychography reconstruction fields
                "final_illumination": [groups["process"] + "final_illumination"],
                "image_x": [groups["process"] + "image_x"],
                "image_y": [groups["process"] + "image_y"],
                "reciprocal_res": [groups["process"] + "reciprocal_resolution"],

                "detector_mask": [groups["detector"] + "detector_mask"]

            }

    def print_groups(self, file_name):

        def print_attrs(name, obj):
            printv(name)
            for key, val in obj.attrs.items():
                printv("    %s: %s" % (key, val))
        
        separator = "----------------------------------------------------"
        printv(separator  + "\n  " +  "Printing all hdf5 groups in " + file_name + " :" + "\n" + separator) 
        try:
            with h5py.File(file_name, "r") as f: 
                f.visititems(print_attrs)
        except IOError:
            printv("***ERROR*** opening file: " + file_name)
            return 0  
        printv(separator)

    def read(self, file_name, data_format = None, data_indexes = ()):

        if data_format is None: 
            data_format = self.metadataFormat 

        data_dictionary = {}
        try:
            with h5py.File(file_name, "r") as f:
                for key, value in data_format.items():
                    for i in value: #specific data can be stored in different fields of a format, this loop handles that 
                        if i in f : #if group exists...
                            data = f[i][data_indexes]
                            data_dictionary.update({key : data})
                            break
            return data_dictionary
        except IOError:
            printv("***ERROR*** opening file: " + file_name)
            return 0 
              
    def write(self, file_name, data_dictionary, data_format = None):

        if data_format is None: 
            data_format = self.metadataFormat 

        with h5py.File(file_name, 'a') as f:
            #data_format is used to generate hdf5 fields 
            for key, value in data_dictionary.items():
                f.create_dataset(data_format[key][0], data = value)


       
    
