#!/usr/bin/env python

import numpy as np
from scipy import misc

class FarField_Propagator():

    def FPropagate(self, x):
        return np.fft.fft2(x, norm="ortho")

    def IPropagate(self, x):
        return np.fft.ifft2(x, norm="ortho")

    def Shift(self, x):
        return np.fft.fftshift(x)


class NearField_Propagator():

    def Initialize(self, data_shape, alpha):

        self.alpha_y = alpha[0]
        self.alpha_x = alpha[1]

        self.exp_alpha_rad = self.Compute_exp_alpha_rad(self.alpha_y, self.alpha_x, data_shape)

        return self        

    #Computes fft2shift( exp( 1j * alpha_y *  y^2) * exp( 1j * alpha_x * x^2) )
    def Compute_exp_alpha_rad(self, alpha_y, alpha_x, shape):

        #If its a 3D shape we take the inner dimensions
        shape = shape[-2:]

        radius_y = (np.ones(shape).T * (np.arange(0, shape[0]) - shape[0]/2.)).T ** 2 #computes y**2
        radius_x = (np.ones(shape) * (np.arange(0, shape[1]) - shape[1]/2.)) ** 2 #computes x**2 

        #FFT will shift the thing later, we fft shift here beforehand to be consistent
        exp_alpha_rad = np.fft.fftshift(np.exp(alpha_y * radius_y) * np.exp(alpha_x * radius_x))

        return exp_alpha_rad

    
    def FPropagate(self, x):
        return np.fft.ifft2( np.fft.fft2(x) * np.conj(self.exp_alpha_rad))

    #propagator=@(x,alpha_x, alpha_y) IFFT2 ( FFT2(x)*fft2shift( exp( 1j * alpha_y *  y^2) * exp( 1j * alpha_x * x^2) ) )
    def IPropagate(self, x):
        return np.fft.ifft2( np.fft.fft2(x) * self.exp_alpha_rad)

    def Shift(self, x):
        return x









    
