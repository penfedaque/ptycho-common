#!/usr/bin/env python

import numpy as np
from scipy import misc
from datetime import datetime
from skimage.restoration import unwrap_phase
from os import path, makedirs
from mpi4py import *

def printd(string):
    print("MPI Rank " + str(MPI.COMM_WORLD.Get_rank()) + ": " + string)

def printv(string):
    if MPI.COMM_WORLD.Get_rank() is 0:
        print(string)

def create_reconstruction_dir():
    base = "results_"
    now = datetime.now()
    sim = str(now.year)[-2:] + str(now.month).zfill(2) + str(now.day).zfill(2) + "_" + str(now.hour).zfill(2) + str(now.minute).zfill(2) + str(now.second).zfill(2)
    folder = base + sim + "/"
    if MPI.COMM_WORLD.Get_rank() is 0:
        if not path.exists(folder): makedirs(folder)
    return folder

results_folder = create_reconstruction_dir()

def image_from_complex(data, fname = "image"):

    misc.imsave(fname + "_abs.tiff", np.absolute(data))

    misc.toimage(np.absolute(data), cmin=0, cmax=140).save(fname + "_abs_rescaled.tiff")
    misc.toimage(np.absolute(data), cmin=0, cmax=np.median(np.absolute(data))).save(fname + "_abs_median.tiff")
    misc.toimage(np.absolute(data), cmin=0, cmax=np.median(np.absolute(data))*2).save(fname + "_abs_median2.tiff")

    misc.imsave(fname + "_real.tiff", np.real(data))
    misc.imsave(fname + "_imag.tiff", np.imag(data))

    phase_data = np.angle(data)

    misc.imsave(fname + "_angle.tiff", phase_data)

    print("-------------------------------------------")
    print(data)
    print("-------------------------------------------")
    print(np.real(data))
    print("-------------------------------------------")
    print(np.imag(data))
    print("-------------------------------------------")
    print(np.absolute(data))
    print("-------------------------------------------")



def crop_reconstruction(reconstruction, crop_y, crop_x):

    return reconstruction[crop_y: reconstruction.shape[0] - crop_y, crop_x: reconstruction.shape[1] - crop_x]

def rescale_reconstruction(reconstruction, image_scale, threshold = 20):

    scale_min = np.amin(image_scale)

    mask = (image_scale < scale_min*threshold)*1.

    max_reconstruction = np.amax(reconstruction * mask)

    reconstruction[mask == 0] = max_reconstruction

    return reconstruction

def phase_unwrap_np_yx(data):

    phase_data = np.angle(data)
    for i in range(0, len(data.shape)):
        phase_data = np.unwrap(phase_data, axis = i)
    return phase_data

def phase_unwrap_ski(data):

    phase_data = np.angle(data)

    return unwrap_phase(phase_data)

def phase_unwrap_np_xy(data):

    phase_data = np.angle(data)
    for i in range(0, len(data.shape)):
        phase_data = np.unwrap(phase_data, axis = len(data.shape)-i-1)
    return phase_data

def phase_unwrap_np_y(data):

    phase_data = np.angle(data)
    return np.unwrap(phase_data, axis = 0)

def phase_unwrap_np_x(data):

    phase_data = np.angle(data)
    return np.unwrap(phase_data, axis = 1)

def phase_unwrap_np_xplusy(data):

    phase_data = np.angle(data)
    phase_data_y = np.unwrap(phase_data, axis = 0)
    phase_data_x = np.unwrap(phase_data, axis = 1)

    return phase_data_x + phase_data_y



    

    
