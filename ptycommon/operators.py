#!/usr/bin/env python

import numpy as np

def Overlap(image, frames, translations, illumination = 1):

    image.fill(0)

    for i in range(frames.shape[-3]):
        image[ int(translations[i, 1]): int(translations[i, 1]) + frames.shape[-2], \
               int(translations[i, 0]): int(translations[i, 0]) + frames.shape[-1]] \
             += frames[i] * illumination

    return image

   





    
