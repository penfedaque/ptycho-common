#!/usr/bin/env python

import numpy as np
from .propagator import *
from .misc import printv, results_folder
import math
from skimage.draw import circle
from mpi4py import MPI
from scipy import misc
from scipy import ndimage

class Preprocessor():

    def set_propagator(self, propagator):
   
        self.Ptor = propagator

    #Generate an initial image using an aproximation of the phase based on the Transport of Intensity Equation (TIE)
    #smaller alphaReg = smoother, energy in KeV, pixel size and dist in cm
    def initial_image_TIE(self, frames, illum_intensities, coordinates, inv_reciprocal_res, pixel_size, energy = 20, propagation_dist = 75, alphaReg = 0.0002):

        translations = ComputeTranslations(coordinates, inv_reciprocal_res)
        translations = ShiftTranslations(translations)
        image_shape = ComputeImageShape(translations, frames.shape)

        image = np.zeros(image_shape, dtype=np.float64)


        #Scale the frames by the flat field (illumination intensities)
        frames_norm = frames[:] / illum_intensities
 
        #care with this guy: it takes the pix size and dist in cm and the energy in KeV
        #Uncomment this below to use the function, disabled for now to avoid the tomopy dependency
        #phase_frames = tomopy.retrieve_phase(frames_norm, pixel_size=pixel_size, dist=propagation_dist, energy=energy, alpha=alphaReg)

        image = Overlap(image, phase_frames, translations)

        #We are overlapping all frames so we normalize based on the number of frames
        image = image / frames.shape[-3]

        #We generate a complex value image based on e^i*phase
        image = np.exp(image*1j)        

        #image = image.reshape(image.shape[-2], image.shape[-1])

        return image

    #Generate an initial image using an aproximation of the phase based on the Transport of Intensity Equation (TIE)
    #smaller alphaReg = smoother, energy in KeV, pixel size and dist in cm
    def initial_illum_TIE(self, data, illum_intensities, pixel_size, energy = 20, propagation_dist = 75, alphaReg = 0.0002):

        illum = np.zeros(data.shape, dtype=np.float64)

        #Scale the frames by the flat field (illumination intensities)
        illum_norm = illum_intensities / data[:]

        illum_norm = illum_norm.reshape(1, illum_norm.shape[-2], illum_norm.shape[-1])
 
        #care with this guy: it takes the pix size and dist in cm and the energy in KeV
        phase_illum = tomopy.retrieve_phase(illum_norm, pixel_size=pixel_size, dist=propagation_dist, energy=energy, alpha=alphaReg)

        phase_illum = phase_illum.reshape(phase_illum.shape[-2], phase_illum.shape[-1])

        #We generate a complex value illum based on e^i*phase
        illum = np.exp(phase_illum*1j) * np.sqrt(data[:])       

        return illum


    def gen_radius(self, size):

        radius_x = (np.array(range(-size[0]//2, size[0]//2))/size[0])**2

        radius_y = (np.array(range(-size[1]//2, size[1]//2))/size[1])**2

        radius = np.zeros(size)

        radius = radius + radius_x.reshape((radius_x.shape[0],1)) 
        radius = radius + radius_y.reshape((1, radius_y.shape[0])) 

        radius = np.sqrt(radius)

        misc.imsave(results_folder + "radius_test.tif", radius)

        return radius

    def gen_object_mask(self, image, threshold = 0.35, kernel_radius = 6):

        ptor = FarField_Propagator()

        constant = np.mean(image)        
        image_abs = np.abs(image - constant)
        radius = self.gen_radius(image.shape)

        mask = ptor.IPropagate( ptor.FPropagate( image_abs )  * np.exp( -0.5 * (ptor.Shift( radius) * kernel_radius) **2 ) )
        
        mask = np.abs(mask)

        binary_mask = mask * 0

        binary_mask[mask > np.amax(mask) * threshold] = 1 

        return binary_mask

    #Factor used in a near-field propagation
    def gen_nf_alpha(self, frame_size_y, frame_size_x, pixel_size, distance, energy):

        numerator = 1j * math.pi *  distance * self.gen_wavelenght(energy) 
 
        alpha_y = numerator / ((pixel_size * frame_size_y)**2)
        alpha_x = numerator / ((pixel_size * frame_size_x)**2)

        return [alpha_y, alpha_x]
    

    #Wavelenght in meters, energy in Joules
    def gen_wavelenght(self, energy):

        h = 6.626070040e-34
        c = 2.99792458e+8

        return h*c/energy


    def gen_reciprocal_resolution(self, frame_size_y, frame_size_x, pixel_size, detector_distance, energy):

       wavelenght = self.gen_wavelenght(energy)

       printv("The wavelenght used is " + str(wavelenght) + " ...")

       reciprocal = [0,0]
       reciprocal[0] = frame_size_x * pixel_size/ (wavelenght * detector_distance)
       reciprocal[1] = frame_size_y * pixel_size/ (wavelenght * detector_distance)

       return reciprocal

    def norm_shifted_ipropagate(self, x):

        return self.Ptor.Shift(self.Ptor.IPropagate(x))

    def norm_shifted_fpropagate(self, x):

        return self.Ptor.Shift(self.Ptor.FPropagate(x))

    def average_3D_arr(self, arr):

        """Average a 3D array over the first (0) axis.
        Args:
            arr (numpy.ndarray(shape =(x,y,z)): Input 3D array.
        Returns:
            (numpy.ndarray(shape =(x,y)) Average in 2D."""

        n = arr.shape[0]
        arr_sum = np.sum(arr, axis=0)
        return arr_sum/n

    def gen_illum(self, avg_frames, t, alpha = 0):
        """Generate an illumination.
        Args:
            avg_frames (numpy.ndarray(shape =(x,y), dtype = float): Average of a stack of frames.
            t (int or float): Threshold.
        Returns:
            (numpy.ndarray(shape =(x,y), dtype = float) Output illumination."""

        msk_avg_frames = avg_frames * (avg_frames > t)

        #Alpha 0 means the initial illumination is on focus, the bigger the alpha the more out-of-focus it gets
        if alpha == 0:
            exp_radius = 1

        else:

            radius = np.array(range(-avg_frames.shape[0]/2, avg_frames.shape[0]/2))
            radius = np.tile(radius, (avg_frames.shape[1], 1))
            radius = radius**2 + radius.transpose()**2

            exp_radius = self.Ptor.Shift(np.exp(radius * 1j * alpha))

        #numpy FFT centers the frequencies, we need an ffshift after the inverse fourier transform
        illumination = self.norm_shifted_ipropagate(np.sqrt(msk_avg_frames) * exp_radius)

        return illumination  


    def gen_pinhole(self, pinhole_radius, illum_h, illum_w):

        pinhole = np.zeros((illum_h, illum_w), dtype=np.float32)

        if pinhole_radius < 5:

            pinhole[illum_h//2 - pinhole_radius: illum_h//2 + pinhole_radius + 1, illum_w//2] = 1
            pinhole[illum_h//2, illum_w//2 - pinhole_radius: illum_w//2 + pinhole_radius + 1] = 1
            pinhole[ illum_h//2 - int(math.ceil(pinhole_radius/2.0)): illum_h//2 + int(math.ceil(pinhole_radius/2.0)) + 1,\
                     illum_w//2 - int(math.ceil(pinhole_radius/2.0)): illum_w//2 + int(math.ceil(pinhole_radius/2.0)) + 1] = 1
        
        else:        

            rr, cc = circle(illum_h//2, illum_w//2, pinhole_radius)
            pinhole[rr, cc] = 1


        return pinhole


    def gen_illum_mask(self, a, t):

        """Generate a mask given an input array and a threshold.
        Args:
            a (array_like): Input data.
            t (int or float): Threshold.
        Returns:
            (array_like of float) Output mask."""

        mask = (a > t)*1.
        return mask 

    def gen_illum_threshold(self, a, scale = 10.):

        """Compute the maximum value of an array an scale it.
        Args:
            a (array_like): Input data.
            scale (float, optional): Scaling factor.
        Returns:
            (float) Maximum value scaled."""

        return np.amax(a)/scale

    #Computes a phase curvature as  exp( 1j * alpha_y *  y^2) * exp( 1j * alpha_x * x^2) 
    def Compute_phase_curvature(self, alpha_y, alpha_x, shape):

        #If its a 3D shape we take the inner dimensions
        shape = shape[-2:]

        radius_y = (np.ones(shape).T * (np.arange(0, shape[0]) - shape[0]/2.)).T ** 2 #computes y**2
        radius_x = (np.ones(shape) * (np.arange(0, shape[1]) - shape[1]/2.)) ** 2 #computes x**2 

        curvature = np.exp(alpha_y * radius_y) * np.exp(alpha_x * radius_x)

        return curvature


    def gen_pinhole_illumination(self, illumination_distance, pinhole_width, energy, data_shape, reciprocal_res):


        printv("Generating initial illumination and mask for a pinhole...")

        pinhole_rad_m = pinhole_width / 2.0


        prop = FarField_Propagator()


        pinhole_rec_resolution = self.gen_reciprocal_resolution(
                          data_shape[1], 
                          data_shape[2], 
                          1/reciprocal_res[0], 
                          illumination_distance, 
                          energy)


        pinhole_radius_p = int(math.ceil(pinhole_rad_m*pinhole_rec_resolution[0]))
        pinhole_mask_radius_p = pinhole_radius_p #+ 106

        pinhole = self.gen_pinhole(pinhole_radius_p, data_shape[1], data_shape[2])
        pinhole_mask = self.gen_pinhole(pinhole_mask_radius_p, data_shape[1], data_shape[2])

        misc.imsave(results_folder + "initial_pinhole.tif", pinhole)

        illumination = prop.Shift(prop.FPropagate(prop.Shift(pinhole)))

        #Apparently we need a phase curvature correction to properly approximate this
        alpha = -1j * math.pi * ((1/reciprocal_res[0])**2) / (illumination_distance * self.gen_wavelenght(energy))
        phase_curvature = self.Compute_phase_curvature(alpha, alpha, data_shape)


        phase_curvature = np.conj(phase_curvature)

        illumination *= phase_curvature

        misc.imsave(results_folder + "pinhole_mask.tif", pinhole_mask)

        pinhole_mask = prop.Shift(pinhole_mask.astype(np.dtype('complex64')))

        return illumination, phase_curvature, pinhole_mask


    def prepare(self, data, metadata, comm, alpha_focus = 0):

        """Complete the metadata in the case that something is missing."""


        #Different propagator used in near- and far-field
        if "near_field" in  metadata:

            printv("Near-field dataset, preparing metadata...")

            nf_alpha = self.gen_nf_alpha(
                                data.shape[1], 
                                data.shape[2], 
                                metadata["x_pixel_size"], 
                                metadata["detector_distance"], 
                                metadata["energy"])

            prop = NearField_Propagator().Initialize(data.shape, nf_alpha)
            self.set_propagator(prop)

            #factor needed for the propagation
            metadata["nf_exp_alpha_rad"] = prop.exp_alpha_rad

        else:

            printv("Far-field dataset, preparing metadata...")
       
            prop = FarField_Propagator()
            self.set_propagator(prop)


        #Before recontruction -> We want Illumination, Frames and Illumination mask centered in real space, but ffshifted in fourier space (frequencies in the corners)
        #We fftshift the frames before generating an initial illumination and mask, so that the three are consistent
        #In near field, the propagator performs no shift.

        printv("Shifting the stack of frames...")
        for n in range(0, data.shape[0]):
            data[n,:,:] = self.Ptor.Shift(data[n,:,:])

        #We need to fftshift this mask, as the frames will be shifted too
        if "detector_mask" in metadata:
             metadata["detector_mask"] = self.Ptor.Shift(metadata["detector_mask"])

        #We shift the illumination intensities
        if "illumination_intensities" in metadata:
             metadata["illumination_intensities"] = self.Ptor.Shift(metadata["illumination_intensities"])

        if "reciprocal_res" not in metadata and not "pix_translations" in metadata:

            printv("Computing the pixel resolution in the image space...")

            #The resolution depends on the detector distance and on the lens (or the illumination "size"). 
            #In the far-field reciprocal-resolution-computation we are assuming a focusing lens, if there is no lens, the near-lens resolution should be used instead
            if "near_field" in  metadata:
                metadata["reciprocal_res"] = [1./metadata["x_pixel_size"], 1./metadata["x_pixel_size"]]
                 
            else:
                metadata["reciprocal_res"] = self.gen_reciprocal_resolution(
                                                  data.shape[1], 
                                                  data.shape[2], 
                                                  metadata["x_pixel_size"], 
                                                  metadata["detector_distance"], 
                                                  metadata["energy"])

        #If illumination is a pinhole, used with CSX data for now
        if "pinhole_width" in metadata:

            printv("Generating initial illumination and mask for a pinhole...")

            metadata["illumination"], metadata["phase_curvature"], metadata["illumination_mask"] = \
                    self.gen_pinhole_illumination(
                         metadata["illumination_distance"], 
                         metadata["pinhole_width"], 
                         metadata["energy"], 
                         data.shape, 
                         metadata["reciprocal_res"])


        #If we use MPI, we need to share the average of the frames to generate an initial illumination or mask
        if "illumination" not in metadata or ("illumination_mask" not in metadata and "near_field" not in  metadata):

            printv("Computing the frames average to prepare additional structures...")

            data.astype(np.dtype('float64'))

            data_sum = np.sum(data, axis=0)

            send_buff = np.array(data_sum, dtype=np.dtype('float32'))
            self. avg_frames = np.array(data_sum, dtype=np.dtype('float32'))

            printv("-----------------------")
            printv(MPI.COMM_WORLD.size)

            if MPI.COMM_WORLD.size > 1:
                MPI.COMM_WORLD.Allreduce(send_buff, self. avg_frames, op=MPI.SUM)

            self. avg_frames = self. avg_frames/float(metadata["all_translations"].shape[0])


        if "illumination" not in metadata:

            printv("Generating an initial illumination with alpha = " + str(alpha_focus))

            #Those scaling numbers (also for the mask) are the hardcoded-magic factors used in SHARP, they have worked well enough for now
            illum_scaling = 20.
            illum_threshold = np.amax(self. avg_frames)/illum_scaling

            metadata["illumination"] = self.gen_illum(self.avg_frames, illum_threshold, alpha_focus)

        #We use no illumination mask in near field
        if "illumination_mask" not in metadata and "near_field" not in  metadata:

            printv("Generating an initial illumination mask...")

            illum_mask_scaling = 60.
            illum_mask_threshold = np.amax(self. avg_frames)/illum_mask_scaling

            metadata["illumination_mask"] = self.gen_illum_mask(self.avg_frames, illum_mask_threshold)










    
