#!/usr/bin/env python

import numpy as np

#This computes the shape of the image based on the shifted (0-based) translations in pixels
def ComputeImageShape(translations, frames_shape):

    max_x = np.amax(translations[:,1])
    max_y = np.amax(translations[:,0])


    return (frames_shape[-2] + max_y, frames_shape[-1] + max_x)


#This computes an array of translations in pixels based on an array of measured coordinates in real space
def ComputeTranslations(coordinates, inv_reciprocal_res):

    translations = (np.rint(coordinates[:,0:2] * inv_reciprocal_res)).astype(int)

    return translations

#Shift translations to 0, 0 based on the minimum values
def ShiftTranslations(translations):

    minimum = [None]*translations.shape[1]

    for i in range(0, translations.shape[1]):
        minimum[i] = np.amin(translations[:,i])
   
    translations = translations - minimum
 
    return translations
   





    
