#!/usr/bin/env python

import subprocess
import operator
import os
import sys
from .misc import printd, printv

#Setup local GPU device based on a local MPI rank
def setup_device(local_rank):

    cmd = "nvidia-smi --query-gpu=index,memory.total,memory.free,memory.used,pstate,utilization.gpu --format=csv"
 
    result = str(subprocess.check_output(cmd.split(" ")))

    if sys.version_info <= (3, 0):
        result = result.replace(" ", "").replace("MiB", "").replace("%", "").split()[1:] # use this for python2
    else:
        result = result.replace(" ", "").replace("MiB", "").replace("%", "").split("\\n")[1:] # use this for python3


    result = [res.split(",") for res in result]
  
    if sys.version_info >= (3, 0):
        result = result[0:-1]

    result = [[int(r[0]), int(r[3])] for r in result]
    result = sorted(result, key=operator.itemgetter(1))

    nvidia_device_order = [res[0] for res in result]

    printv("GPU devices occupancy order:\n")
    printv(str(nvidia_device_order))
      
    os.environ["CUDA_VISIBLE_DEVICES"] = str(nvidia_device_order[local_rank % len(nvidia_device_order)])
    printd("GPU visible devices for this process: " + str(os.environ["CUDA_VISIBLE_DEVICES"]))

